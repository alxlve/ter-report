\phantomsection
\thispagestyle{partfix}
\part{Architecture et découpage modulaire}

Ici, nous allons étudier l'architecture et le découpage du projet. Nous verrons comment les classes métiers communiquent entre elles.



\section{Structure de graphe}
Le projet étant principalement axé sur un traitement de graphes, il nous parut évident que nous devions développer une structure afin de représenter ceux-ci.\\

Les graphes sont généralement représentés sous 3 formes différentes, nous discuterons ci-dessous des avantages et des inconvénients que chaque modèle implique.


\subsection{Matrice d'adjacence}
Cette représentation à l'avantage de permettre d'accéder très facilement et très rapidement à un nœud donné, pour peu qu'on l'on ait connaissance de l'indice de celui-ci dans la matrice. Cependant elle implique un surcoût négatif et conséquent sur la quantité de mémoire nécessaire à son exécution.\\
La mémoire étant une des problématiques de notre projet, nous avons choisis d'écarter cette solution.\\

Enfin comme nous n'avions pas la nécessité de pouvoir accéder à des nœuds selon leur indice, l'utilité d'utiliser cette structure était très limitée voir nulle.


\subsection{Liste de liens}
Une liste de lien (ou \english{edge list} en anglais) représente un graphe à partir de ses arcs. Étant donné que notre travail consistait à effectuer des opérations sur des arcs, cette représentation disposait d'un intérêt certain, mais elle n'a pas été retenue.


\subsection{Liste d'adjacence}
La liste d'adjacence est la représentation que nous avons choisie. C'est celle qui nous semblait la plus à même de répondre à notre problématique.
Elle est assez légère en mémoire, et offre des performances satisfaisantes, notamment pour des algorithmes de parcours des voisins.



\section{Les tâches}
Afin de paralléliser les traitements, nous avons découpé le travail en tâches. Une tâche est représentée par la classe \texttt{Task}, qui, fondamentalement, désigne un intervalle (au sens mathématique du terme) qui identifie les nœuds qui doivent être traités.\par
\n
Les tâches sont générées par le graphe, et une tâche ne peut être générée qu'une seule fois par traitement. En effet, puisque plusieurs \english{threads} travaillent sur un même graphe, le danger était qu'au moins deux \english{threads} reçoivent la même tâche.
Ainsi il nous a fallu utiliser un mutex afin d'assurer la distribution correcte des tâches.


\subsection{Les \english{workers}}
Les \english{workers} sont des processus qui, comme leur nom l'indique, vont travailler. Pour effectuer un calcul, un \english{worker} devra demander au graphe une tâche. Lors de la demande d'une tâche, il existe deux cas de figure : si la demande échoue, une exception de type \texttt{NoTaskAvailable} est jetée. Cela signifie que le travail est terminé, et que le \english{thread} se termine. Sinon, le \english{worker} reçoit une liste de nœuds à traiter, et il calcule les coefficients de \english{clustering} partiel du graphe.


\subsection{Les coefficients de \english{clustering}}
Afin de stocker les résultat des \english{workers}, nous avions besoin d'une structure de données comptabilisant la valeur des coefficients. Cette structure représente simplement un tableau, de taille \texttt{nombreDenœuds}. Lorsqu'un \english{worker} doit incrémenter un coefficient, il doit prévenir cette structure.

Une fois de plus, puisque plusieurs \english{workers} peuvent envoyer un message en même temps à cette structure, il a fallu utiliser des types atomiques (\texttt{std::atomic<int>}) afin que toutes les opérations d'incrémentation se réalisent correctement.

\begin{figure}[!ht]
    \centering
    \includeplainsvg[scale=0.8]{architecture-decoupage}
    \caption{Communication entre les différents processus}
\end{figure}



\section{Gestion de la mémoire}
Étant donné que les données en entrée ne pourront pas tenir en mémoire dans la plupart des cas, il faut trouver des stratégies pour gérer cette mise en mémoire fragmentée, ainsi qu'un format de fichier optimisé pour limiter le plus possible les accès au disque.\\

Pour éviter de saturer la mémoire, il faut donc au préalable créer un cache d'une taille fixe dans la mémoire, composé d'une suite de blocs de données.


\subsection{Structure en blocs}
Dans le cache, nous avons une structure en blocs, configurable au sein de l'application.
Chaque bloc contient un tableau de nœuds triés dans l'ordre croissant sans écart entre deux nœuds, c'est-à-dire que si le bloc possède les nœuds de 100 à 400, le nœud suivant 349 sera forcément 350.\\

Quand on termine de traiter des identifiants de l'un des blocs du cache, ou si l'on ne peut pas continuer puisque les nœuds nécessaires ne sont pas chargés, on libère les données terminées et on place les identifiants restants dans un bloc spécial appelé la \og réserve\fg{}.


\subsection{Réserve}
La réserve est un bloc qui contient également un tableau de nœuds triés dans l'ordre croissant, sauf qu'il peut y avoir des écarts entre deux nœuds.
Ces nœuds sont en attente pour un prochain traitement de la part des \english{workers}, et ils sont prioritaires sur les prochains nœuds à charger.\\

Il faut vider la réserve le plus rapidement possible avant de charger plus de nœuds dans le cache.



\section{Formats de fichiers reconnus}
Notre application doit pouvoir charger plusieurs types de fichiers, plus ou moins \og performants \fg{}.


\subsection{Format ASCII compatible générateur}
Pour effectuer nos tests, sans prendre l'optimisation en compte, nous devions nous servir de graphes pré-générés ou générables à l'aide d'un outil spécialisé dans la génération de graphes de réseaux sociaux.
Nous devons donc directement lire l'entrée du fichier ASCII généré et l'interpréter comme un graphe.

\begin{verbbox}[\small]
1 2
1 3
1 4
1 5
2 1
3 1
...
\end{verbbox}

\begin{figure}[!ht]
    \centering
    \theverbbox
\end{figure}

Chaque ligne du fichier est composée de deux nombres: le premier étant l'identifiant du nœud auquel attacher un voisin, le deuxième étant l'identifiant du voisin.
Ce format de fichier n'est absolument pas optimisé, et le format ASCII nous fait perdre beaucoup de place en disque, surtout pour des graphes de taille élevée.\\

Nous considérons ce format de fichier uniquement car c'est le format de sortie du générateur.


\subsection{Format binaire optimisé pour le programme}

Comme nous venons de voir que le format ci-dessous n'est pas viable, il nous a été nécessaire de trouver une façon beaucoup plus optimisée pour le stockage des informations d'un graphe.
Pour des raisons évidentes de taille, nous avons préféré le format binaire.\\

Le fichier est constitué d'un en-tête sur 512 octets, suivi d'une liste de définitions de nœuds.

\begin{verbbox}[\small]
En-tête
nœud 1
nœud 2
...
\end{verbbox}

\begin{figure}[!ht]
    \centering
    \theverbbox
\end{figure}

L'en-tête comporte un code qui identifie le graphe comme un graphe compatible avec notre programme, suivi de la taille du graphe encodée sur 64 bits ainsi que de la place libre pour des données futures.
La définition d'un nœud comporte l'identifiant du nœud sur 64 bits, le nombre de voisins sur 64 bits, ainsi que la liste des identifiants des voisins sur 64 bits également.\\

Côté application, lors du chargement du fichier binaire, on pré-calcule une table d'indices pour avoir un accès beaucoup plus rapide à certaines sections du fichier, l'écart entre les pointeurs étant configurables.

On peut aisément convertir un fichier ASCII en binaire directement depuis l'application.

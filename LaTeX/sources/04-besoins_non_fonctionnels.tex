\phantomsection
\thispagestyle{partfix}
\part{Besoins non fonctionnels}

Un projet de cette envergure implique forcément certains besoins afin de mener à bien la recherche. Les besoins non fonctionnels sont des besoins qui ne sont pas relatifs au code en lui même mais à l'environnement de travail. Ces besoins formeront une base qui nous permettra de travailler le plus efficacement possible.

Dans cette partie, nous listerons les besoins non fonctionnels relatifs au projet. Nous feront également part des problèmes techniques et des différents tests que ces besoins ont amenés.



\section{Définition des besoins}


\subsection{Environnement de développement}
Nous avons choisi le langage \textsl{C++}, et ce pour plusieurs raisons. Il s'agit d'un langage performant, qui permet une gestion manuelle de la mémoire, offre un accès bas niveau au processeur (via les instructions \glsur{simd}), ainsi que d'autres fonctionnalités pratiques et puissantes (comme les \english{\english{threads}} ou encore les fonctions asynchrones).\n

Deux bibliothèques sont utilisées au sein du projet. Elles appartiennent à la collection de bibliothèques \textsl{Boost}, collection connue et reconnue dont certains aspects ont été directement intégrés dans les dernières révisions \textsl{C++} (\textsl{C++11} et \textsl{C++14}).\n

Tout d'abord, nous nous servons de \textsl{\textbf{Boost.LockFree}}.\\
Cette bibliothèque nous offre des structures qui permettent d'avoir des accès concurrents sur les données sans bloquer ces accès (sans lock / lock-free).\n

Ensuite, nous utilisons \textsl{\textbf{Boost.Program\_options}}.\\
Cette petite bibliothèque sert à gérer de façon pratique, efficace et simplifiée les arguments en ligne de commande. Elle permet entre autre l'affichage d'une liste d'options auto-générée sans un seul \texttt{std::cout}, ainsi que la reconnaissance automatique des arguments entrés.


\subsection{Travail d'équipe}
Pour gérer le travail en équipe, nous avons décidé de nous servir d'un système de versionnage décentralisé : \textsl{Git}.\\
Chaque membre du groupe possède sa copie du dépôt principal (\english{fork}), où il peut décider de créer des branches pour organiser son travail.\n

Autour du système de versionnage, on utilise une interface web simple et pratique pour gérer son projet : \textsl{BitBucket}.\n

\textsl{BitBucket}, nous permet d'effectuer des \og \english{pull request} \fg{}.\\
Un membre du projet propose ses modifications sur le dépôt principal, et le mainteneur (chef de projet) décide de fusionner ou non les modifications proposées.\n

Cette technique permet un contrôle de qualité tout au long du projet, et de bien voir l'évolution du code (qui a fait quoi et quand).


\subsection{Concepts de programmation}
Deux aspects sont abordés dans le projet pour des raisons de rapidité et d'efficacité.\\
Tout d'abord, la programmation parallèle, puis la programmation vectorielle.\n

Le concept de programmation parallèle consiste à exécuter plusieurs instructions en parallèle pour utiliser les $n$-cœurs du processeur, afin d'accélérer l'exécution du programme.\n

Le \textsl{C++} nous fournit, au sein de sa bibliothèque standard, les \english{threads}, les \english{promises} et les \english{futures}.
Les \english{threads} sont des processus indépendants qui s'exécutent en parallèle.\\
Les \english{promises} et les \english{futures} servent à effectuer des opérations asynchrones, de façon à pouvoir réaliser une autre tâche en attendant la complétion d'une autre.\\
Malheureusement, l'utilisation des \english{threads} pose un problème épineux : nous devons les synchroniser.\\

Le concept de programmation vectorielle consiste, dans le cadre de ce projet, en des opérations vectorielles, avec par exemple l'addition de plusieurs entiers en une seule opération, à l'aide des \english{intrinsics} \glsur{sse}. Cela peut grandement accélérer les calculs. En effet, là où une instruction classique consomme un temps \glsur{cpu} pour une addition, une instruction \glsur{simd} prend le même temps pour additionner 4 entiers.


\subsection{Représentation des données}
La structure de base à représenter dans le projet est une structure de graphe.\\
Pour des raisons de performance, un nœud est représenté par un identifiant unique (entier), ainsi qu'un tableau fixe contenant les identifiants des voisins.\\

Le graphe lui est représenté par des listes d'adjacence, beaucoup plus économes en mémoire que la représentation en matrices d'adjacence.\\

Ces graphes doivent pouvoir être importés et exportés. Nous avons pour cela choisi un format de fichier binaire, pour utiliser le moins d'espace disque possible.\\

Un fichier représentant un graphe se présente sous cette forme :

\medskip
\begin{itemize}
    \item Un \english{header} (identifiant du format de fichier)
    \item Le nombre de nœuds du graphe
    \item La définition d'un nœud et de ses voisins sous la forme:

    \begin{itemize}
        \item ID du nœud
        \item Nombre de voisins
        \item Liste d'IDs des voisins
    \end{itemize}
\end{itemize}



\section{Problèmes techniques, tests et essais}


\subsection{Comment représenter un graphe ?}
Notre première contrainte a été de trouver une représentation efficace et économe pour un graphe. Quelques bibliothèques \textsl{C++} étant disponibles, notre première idée fut tout naturellement de nous tourner vers celles-ci.\\

Deux bibliothèques ont retenu notre attention :

\medskip
\begin{itemize}
    \item \textsl{\gls{bgl}}, au sein de la collection \textsl{Boost}
    \item \textsl{Lemon}
\end{itemize}
\medskip

Malheureusement, aucune des deux ne put convenir aux impératifs du projet.\\
\gls{bgl} est trop générique, et la généricité implique toujours de la complexité non nécessaire lors de l'utilisation.\\

\textsl{Lemon} n'est par contre absolument pas adaptée à ce projet. Tandis que \gls{bgl} a l'avantage d'être conforme aux normes du standard \textsl{C++}, \textsl{Lemon} à l'inverse en fait abstraction (par exemple, les itérateurs de \textsl{Lemon} sont inutilisables dans les algorithmes de la bibliothèque standard \textsl{C++}).


\subsection{Contraintes de temps}
Nous avons énormément de pistes à explorer, et ce dans un temps très restreint. Il est donc impossible de traiter le projet dans son intégralité. C'est pourquoi nous avons donc des concessions à faire et nous devons établir une priorisation des différents besoins fonctionnels.


\subsection{Grande variété de graphes}
Il existe différents types de graphes, mais il n'existe pas de modèle de calcul optimal pour tous les types de graphes.\\

Par exemple, les instructions \glsur{simd} (programmation vectorielle) seront intéressantes sur des graphes d'une taille raisonnable ou grande, mais inefficaces sur des graphes de plus petites tailles.
C'est pourquoi il faut prévoir des heuristiques pour chaque type de graphe que l'on peut rencontrer.

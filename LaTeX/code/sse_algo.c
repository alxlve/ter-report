int intersect(short *A, short *B, int l_a, int l_b, short* C) {
  int count = 0;
  short i_a = 0, i_b = 0;
  
  while(i_a < l_a && i_b < l_b) {
    // 1. Load the vectors
    __m128i v_a = _mm_load_si128((__m128i*)&A[i_a]);
    __m128i v_b = _mm_load_si128((__m128i*)&B[i_b]);
    
    // 2. Full comparison
    __m128i res_v = _mm_cmpestrm(v_b, 8, v_a, 8,
				 _SIDD_UWORD_OPS|_SIDD_CMP_EQUAL_ANY|_SIDD_BIT_MASK);
    int r = _mm_extract_epi32(res_v, 0);
    unsigned short a7 = _mm_extract_epi32(v_a, 7);
    unsigned short b7 = _mm_extract_epi32(v_b, 7);
    A += ( a7 <= b7 ) * 8;
    B += ( a7 >= b7 ) * 8;
    
    // 3. Write back common values
    __m128i p = _mm_shuffle_epi8(v_a, sh_mask[r]);
    _mm_storeu_si128((__m128i*)&C[count], p);
    count += _mm_popcnt_u32(r);
  }
  
  return count;
}
